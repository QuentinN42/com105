# Questions (pour le détecteur à seuil)

## Déterminer, en fonction de $\alpha$, $A$ et $N_0$, l’expression des probabilités d’erreur suivantes :

### $P_e(A, A)$

![](https://radosgw.rezel.net/codimd/uploads/upload_522d8c04eb1321118408daf5302cd200.png)

Quand le signal passe de $n-1$ a $n$, augmente de $A\alpha$.
Il y a donc une certaine probabilité (ici en rouge) de dépasser $0$ et donc de générer une erreur.

$$
    P_e(A, A) = \int_{-\infty}^{A(1+\alpha)} G_{N_0/2}(x) dx \\
    = \int_{-\infty}^{A(1+\alpha)}
        \sqrt{\frac{2}{2N_0\pi}} e^{-\frac{x^2}{N_0}}
        dx \\
    = \sqrt{\frac{2}{N_0}} \int_{-\infty}^{A(1+\alpha)}
        \frac{1}{\sqrt{2\pi}} e^{-(x\sqrt{\frac{2}{N_0}})^2/2}
        dx
$$
Soit $X = x\sqrt{\frac{2}{N_0}}$ alors :
$$
    P_e(A, A)
    = \int_{-\infty}^{\frac{A(1+\alpha)\sqrt{2}}{\sqrt{N_0}}}
        \frac{1}{\sqrt{2\pi}} e^{-X^2/2}
        dX \\
    = Q(A(1+\alpha)\sqrt{\frac{2}{N_0}})
$$



### $P_e(A, -A)$

![](https://radosgw.rezel.net/codimd/uploads/upload_92ed03063201c2bb953260db40869578.png)

Quand le signal passe de $A$ a $-A$, il est aténué par le coeficient $\alpha$ et n'atteind pas $-A$. Il y a donc une certaine probabilité (ici en rouge) de dépasser $0$ et donc de générer une erreur.

Cette probabilité ce calcule de la même maniere que precedement en remplacent $1+\alpha$ par $1-\alpha$. Donc :
$$
    P_e(A, -A) = Q(A(1-\alpha)\sqrt{\frac{2}{N_0}})
$$

## Déterminer également $P_e(-A, -A)$ et $P_e(-A, A)$

Par symétrie :
$P_e(-A, -A) = P_e(A, A)$ et $P_e(-A, A) = P_e(A, -A)$.


## Déterminer la probabilité d’erreur par symbole $P_e$ pour les symboles $n= 1,2,...$ en fonction de $\alpha$, $A$ et $N_0$.

En utilisant les probabilités totales :
$$
    P_e =
        P_e(A,A)\times\mathbb{P}(s_n=A, s_{n-1}=A) +
        P_e(A,-A)\times\mathbb{P}(s_n=A, s_{n-1}=-A) +\\
        P_e(-A,A)\times\mathbb{P}(s_n=-A, s_{n-1}=A) +
        P_e(-A,-A)\times\mathbb{P}(s_n=-A, s_{n-1}=-A) \\
    =
        P_e(A,A)/4 +
        P_e(A,-A)/4 +
        P_e(-A,A)/4 +
        P_e(-A,-A)/4 \\
    =
        P_e(A,A)/2 +
        P_e(A,-A)/2 \\
    = \frac{
        Q(A(1+\alpha)\sqrt{2/N_0}) +
        Q(A(1-\alpha)\sqrt{2/N_0})
        }{2}\\
$$

## Déterminer la probabilité d’erreur par symbole $P_e$ pour les symboles $n= 1,2,...$ en fonction de $\alpha$, $E_b/N_0$.

En sachant que $E_b = E_s$ et avec $E_s$ suivant :
$$
    E_s = \frac{|-A|^2 + |A|^2}{2} = A^2
$$

On a :
$$
    P_e = \frac{
        Q((1+\alpha)\sqrt{2E_b/N_0}) +
        Q((1-\alpha)\sqrt{2E_b/N_0})
        }{2} \\
$$

## Donner une expression approchée $P_e$ pour les symboles $n= 1,2,...$ en ne conservant que le terme dominant à fort $E_b/N_0$.

Comme vu en question 1 sur les schemas, $Q((1+\alpha)\sqrt{2E_b/N_0}) << Q((1-\alpha)\sqrt{2E_b/N_0})$, donc on peut approximer :
$$
    P_e \simeq \frac{Q((1-\alpha)\sqrt{2E_b/N_0})}{2}
$$






# Questions (pour le détecteur à retour de décision) :


## Exprimer $P_{e,0}$ en fonction de $E_b/N_0$.

Comme fait dans la question 1 mais avec un facteur $1$ a la place de $1+\alpha$ :
$$
    P_{e,0} = Q(\sqrt{2E_b/N_0})
$$

## Exprimer $z_n$ en fonction de $\alpha$, $s_n$, $s_{n−1}$ et $w_n$ lorsque $\hat{s}_{n−1} \neq s_{n−1}$.

Par reinjection, on a :
$$
    z_n = s_n - \alpha \hat{s}_{n-1} + \alpha s_{n-1} + w_n \\
    z_n = s_n + \alpha s_{n-1} + \alpha s_{n-1} + w_n \\
    z_n = s_n + 2 \alpha s_{n-1} + w_n \\
$$

## Exprimer $P_{e,n}$ en fonction de $P_{e,n-1}$, $\alpha$ et $E_b/N_0$, pour tout $n$

En réutilisant les probabilités totales et graces aux questions precedentes (en prenant $\alpha_\text{precedent} = 2\alpha$), on a :
$$
    P_{e,n} = P_{e,n-1} \times \frac{Q((1-2\alpha)\sqrt{2E_b/N_0}) + Q((1+2\alpha)\sqrt{2E_b/N_0})}{2} + (1-P_{e,n-1}) \times Q(\sqrt{2E_b/N_0})
$$



## Exprimer $P_{e,n}$ en fonction de $\alpha$, $n$ et $E_b/N_0$.
$$
    P_{e,n} = P_{e,n-1} \times (\frac{Q((1-2\alpha)\sqrt{2E_b/N_0}) + Q((1+2\alpha)\sqrt{2E_b/N_0})}{2} - Q(\sqrt{2E_b/N_0})) + Q(\sqrt{2E_b/N_0})
$$
Posons :
- $P_{e,n} = u_n$
- $\frac{Q((1-2\alpha)\sqrt{2E_b/N_0}) + Q((1+2\alpha)\sqrt{2E_b/N_0})}{2} - Q(\sqrt{2E_b/N_0}) = q$
- $Q(\sqrt{2E_b/N_0}) = c$

On a alors a résoudre la récurence :
$$
    u_n = q u_{n-1} + c
$$
Qui a pour solution :
$$
    u_n = c \frac{q^n - 1}{q - 1} + u_0 q^n
$$
En réinjectant, on obtient donc :
$$
    P_{e,n} = Q(\sqrt{2E_b/N_0}) \frac{(\frac{Q((1-2\alpha)\sqrt{2E_b/N_0}) + Q((1+2\alpha)\sqrt{2E_b/N_0})}{2} - Q(\sqrt{2E_b/N_0}))^n - 1}{\frac{Q((1-2\alpha)\sqrt{2E_b/N_0}) + Q((1+2\alpha)\sqrt{2E_b/N_0})}{2} - Q(\sqrt{2E_b/N_0}) - 1} +\\
    Q(\sqrt{2E_b/N_0}) (\frac{Q((1-2\alpha)\sqrt{2E_b/N_0}) + Q((1+2\alpha)\sqrt{2E_b/N_0})}{2} - Q(\sqrt{2E_b/N_0}))^n
$$




## En ne conservant que le terme dominant dans $P_{e,n}$, comparer la probabilité d’erreur approchée obtenue à celle de la question 5.
On a :
$$
    Q((1-2\alpha)\sqrt{2E_b/N_0}) >> Q(\sqrt{2E_b/N_0}) >> Q((1+2\alpha)\sqrt{2E_b/N_0})
$$
Donc on ne garde que $Q((1-2\alpha)\sqrt{2E_b/N_0})$ :
$$
    P_{e,n} = Q(\sqrt{2E_b/N_0}) (\frac{\frac{Q((1-2\alpha)\sqrt{2E_b/N_0})^n}{2^n} - 1}{\frac{Q((1-2\alpha)\sqrt{2E_b/N_0})}{2} - 1} + \frac{Q((1-2\alpha)\sqrt{2E_b/N_0})^n}{2^n})
$$

On peut encor simplifier pour obtenir:
$$
    P_{e,n} \simeq Q(\sqrt{2E_b/N_0}) \frac{\frac{Q((1-2\alpha)\sqrt{2E_b/N_0})^n}{2^n} - 1}{\frac{Q((1-2\alpha)\sqrt{2E_b/N_0})}{2} - 1} \\
    \simeq Q(\sqrt{2E_b/N_0}) \sum_{k=0}^n (\frac{Q((1-2\alpha)\sqrt{2E_b/N_0})}{2})^k \\
    \simeq Q(\sqrt{2E_b/N_0}) (1 + \sum_{k=1}^n (\frac{Q((1-2\alpha)\sqrt{2E_b/N_0})}{2})^k) \\
    \simeq Q(\sqrt{2E_b/N_0})
$$




